package com.rs.logutil;

import android.util.Log;

public class LogDebug {

    private static final String TAG="Hello Log";

    public static void d(String message){
        Log.d(TAG, message);
    }
}
