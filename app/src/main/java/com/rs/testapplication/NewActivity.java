package com.rs.testapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class NewActivity extends AppCompatActivity {
    Button mPrev;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("Activity2 : OnCreate");
        mPrev = findViewById(R.id.next);
        mPrev.setText("Previous");
        mPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("Activity2 : OnStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("Activity2 : OnResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("Activity2 : OnPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("Activity2 : OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("Activity2 : OnDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("Activity2 : OnRestart");
    }
}
