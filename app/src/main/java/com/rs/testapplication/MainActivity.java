package com.rs.testapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button mNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("Activity1 : OnCreate");
        mNext = findViewById(R.id.next);
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewActivity.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("Activity1 : OnStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("Activity1 : OnResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("Activity1 : OnPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("Activity1 : OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("Activity1 : OnDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("Activity1 : OnRestart");
    }
}